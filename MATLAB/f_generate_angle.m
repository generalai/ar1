function PULSE=f_generate_angle(PULSE,current_time_idx)
    if (current_time_idx>=PULSE.start_time_idx) && (current_time_idx<=PULSE.end_time_idx)
        idx=current_time_idx-PULSE.start_time_idx+1;
        PULSE.in.acc=PULSE.in.acc+PULSE.unit_acc_prime_array(PULSE.pulse_seq_idx,idx)*PULSE.acc_differential;
        PULSE.in.vel=PULSE.in.vel+PULSE.in.acc;
        PULSE.in.pos=PULSE.in.pos+PULSE.in.vel;
    else
        PULSE.in.acc=PULSE.in.acc;
        PULSE.in.acc=PULSE.in.acc;
        PULSE.in.pos=PULSE.in.pos;
    end
end