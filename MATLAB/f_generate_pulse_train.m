function PULSE = f_generate_pulse_train(acc_segment_length_vector,delay_vector,pulse_group_delay,angle_magnitudes)

% delay between pulses within same group
PULSE.delay_vector=delay_vector;
%delay between pulse groups
PULSE.pulse_group_delay=pulse_group_delay;
%current pulse index within pulse group
PULSE.pulse_seq_idx=1;
%curernt acceleration
PULSE.in.acc=0;
%current velocity
PULSE.in.vel=0;
%current position
PULSE.in.pos=0; %measurd in degrees
%for static amplitude pulses, specify final position angle
PULSE.angle_magnitudes=angle_magnitudes;
% Jerk control waveform
% This vector specifies the gradual changes in acceleration
PULSE.unit_acc_prime_array=[];
%Given desired position and time deltas, this is the calculated 
% acceleration delta
PULSE.acc_differential=[];
%remembers the last position angle
PULSE.prev_angle=0;
for i=1: length(acc_segment_length_vector)
    v=[ ones(1,acc_segment_length_vector(i)) ...
        zeros(1,acc_segment_length_vector(i)) ...
        -1*ones(1,acc_segment_length_vector(i)) ...
        -1*ones(1,acc_segment_length_vector(i)) ...
        zeros(1,acc_segment_length_vector(i)) ...
        ones(1,acc_segment_length_vector(i))];
    PULSE.unit_acc_prime_array(i,1:length(v))=v;% derivative of acceleration profile
end
%given a dACC/dt vector of unit amplitude calculate the position vector
PULSE.position_vector=cumsum(cumsum(cumsum(PULSE.unit_acc_prime_array,2),2),2);% shape of angle position command
%Use this value to finc acc_differential
PULSE.unit_position_mag=(PULSE.position_vector(:,end))';
% calculate length of pulses
n=PULSE.unit_acc_prime_array'~=0;
[dummy,Index]=sort(n);
PULSE.acc_prime_len_vector=Index(end,:).*any(n);
%Start time index of curernt pulse
PULSE.start_time_idx=PULSE.delay_vector(1)+1;
%end time index of current pulse
PULSE.end_time_idx=(PULSE.acc_prime_len_vector(PULSE.pulse_seq_idx)+PULSE.delay_vector(PULSE.pulse_seq_idx));
