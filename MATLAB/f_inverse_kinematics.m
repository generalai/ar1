function JOINT_ANGLES = f_inverse_kinematics(alphaang,betaang,mode)
%input angles are in radians
    switch mode
        case 'both_legs'
            JOINT_ANGLES.lhiprho=lhr(alphaang,betaang);
            JOINT_ANGLES.lhiptheta=lhth(alphaang,betaang);
            JOINT_ANGLES.ltransfem=ltf(alphaang,betaang);
            JOINT_ANGLES.rhiprho=rhr(alphaang,betaang);
            JOINT_ANGLES.rhiptheta=rhth(alphaang,betaang);
            JOINT_ANGLES.rtransfem=rtf(alphaang,betaang);
        case 'left_leg'
            JOINT_ANGLES.lhiprho=lhr(alphaang,betaang);
            JOINT_ANGLES.lhiptheta=lhth(alphaang,betaang);
            JOINT_ANGLES.ltransfem=ltf(alphaang,betaang);  
            JOINT_ANGLES.rhiprho=0;
            JOINT_ANGLES.rhiptheta=0;
            JOINT_ANGLES.rtransfem=0; 
         case 'right_leg'
            JOINT_ANGLES.lhiprho=0;
            JOINT_ANGLES.lhiptheta=0;
            JOINT_ANGLES.ltransfem=0;
            JOINT_ANGLES.rhiprho=rhr(alphaang,betaang);
            JOINT_ANGLES.rhiptheta=rhth(alphaang,betaang);
            JOINT_ANGLES.rtransfem=rtf(alphaang,betaang);            
    end
end

function [lhiprho]=lhr(alpha,beta)
% given torso alpha and beta euler angles, find angle position of left hip
% rho joint
    b0_f=   0;
    b1_f=  0.3086*alpha+1.091;
    b2_f= -0.0005593*alpha-0.2388;
    b3_f=  0.002121*alpha+0.0573;
    b4_f=  0.003227*alpha-0.005891;
    f1=b0_f + b1_f.*sin(beta*1.6) + b2_f.*sin(2*beta*1.6) + b3_f.*sin(3*beta*1.6) + b4_f.*sin(4*beta*1.6);

    p00 =  -2.802e-05;
    p10 =  -0.001211;
    p01 =  -0.0001526;
    p20 =   5.361e-05;
    p11 =   0.00257;
    p02 =  -0.002823;
    p30 =   0.001641;
    p21 =   0.001146;
    p12 =   0.1355;
    p03 =   0.01039;
    p40 =  -9.583e-05;
    p31 =   5.721e-05;
    p22 =   0.00668;
    p13 =  -0.2001;
    p50 =  -0.0002324;
    p41 =  -0.00261;
    p32 =  -0.1849;
    p23 =  -0.04207;

    f2=p00 + p10.*beta + p01.*alpha + p20.*beta.^2 + p11.*beta.*alpha + p02.*alpha.^2 + p30.*beta.^3 + p21.*beta.^2.*alpha ... 
       + p12.*beta.*alpha.^2 + p03.*alpha.^3 + p40.*beta.^4 + p31.*beta.^3.*alpha + p22.*beta.^2.*alpha.^2 + p13.*beta.*alpha.^3 ...
       + p50.*beta.^5 + p41.*beta.^4.*alpha + p32.*beta.^3.*alpha.^2 + p23.*beta.^2.*alpha.^3;

    lhiprho=f1+f2;
end
function [lhiptheta]=lhth(alpha,beta)
% given torso alpha and beta euler angles, find angle position of left hip
% theta joint
    a0_f= -0.6712*alpha+0.3276;
    a1_f=  0.1154*alpha-0.3268;
    b1_f=  8.321e-5*alpha-4.192e-5;
    w_f =  0.5982*alpha+1.551; 
    f1=a0_f + a1_f.*cos(beta.*w_f) + b1_f.*sin(beta.*w_f);

    p00 =  -0.0004367;
    p10 =    2.43e-05;
    p01 =   -0.003753;
    p20 =   -0.001103;
    p11 =  -0.0003251;
    p02 =     0.05319;
    p21 =  -7.895e-05;
    p12 =   -0.003437;
    p03 =      0.2929;
    p22 =     0.08558;
    p13 =     0.02703;
    p04 =      0.1488;

    f2=p00 + p10.*beta + p01.*alpha + p20.*beta.^2 + p11.*beta.*alpha + p02.* ...
        alpha.^2 + p21.*beta.^2.*alpha + p12.*beta.*alpha.^2 + p03.*alpha.^3 + ...
        p22.*beta.^2.*alpha.^2 + p13.*beta.*alpha.^3 + p04.*alpha.^4;
    lhiptheta=f1+f2;
end
function [ltransfem]=ltf(alpha,beta)
% given torso alpha and beta euler angles, find angle position of left
% transverse femeral joint
    a0_f= 0.03519*alpha-0.01945;
    a1_f=-0.05463*alpha+0.02965;
    a2_f= 0.02434*alpha-0.0126;
    a3_f=-0.004937*alpha+0.002318;
    b1_f= 0.4815*alpha+0.8143;
    b2_f=-0.3412*alpha-0.1265;
    b3_f= 0.1749*alpha+0.06443;
    f1=a0_f + a1_f.*cos(beta) + b1_f.*sin(beta) + a2_f.*cos(2*beta) + b2_f.*sin(2*beta) ...
        + a3_f.*cos(3*beta) + b3_f.*sin(3*beta);

    p00 =  -3.372e-06;
    p10 =  -0.0009872;
    p01 =  -0.0001813;
    p20 =  -1.553e-06;
    p11 =    0.001626;
    p02 =    0.009834;
    p21 =  -3.131e-06;
    p12 =      0.1419;
    p03 =     0.01447;
    p22 =   -0.001454;
    p13 =     -0.1217;
    p04 =     -0.7112;
    f2=p00 + p10.*beta + p01.*alpha + p20.*beta.^2 + p11.*beta.*alpha + p02.* ...
        alpha.^2 + p21.*beta.^2.*alpha + p12.*beta.*alpha.^2 + p03.*alpha.^3 + ...
        p22.*beta.^2.*alpha.^2 + p13.*beta.*alpha.^3 + p04.*alpha.^4;
    ltransfem=f1+f2;
end
function [rhiprho]=rhr(alpha,beta)
% given torso alpha and beta euler angles, find angle position of right hip
% rho joint
    b0_f=   0;
    b1_f=  -0.3347*alpha+1.095;
    b2_f=   0.0296*alpha-0.2431;
    b3_f=  -0.01745*alpha+0.05952;
    b4_f=  0.0002579*alpha-0.006372;
    f1=b0_f + b1_f.*sin(beta*1.6) + b2_f.*sin(2*beta*1.6) + b3_f.*sin(3*beta*1.6) + b4_f.*sin(4*beta*1.6);

    p00 =   2.709e-06;
    p10 =  -0.0009122;
    p01 =   -4.13e-06;
    p20 =   4.788e-05;
    p11 =   -0.002443;
    p02 =  -0.0007193;
    p30 =     0.00151;
    p21 =  -0.0005303;
    p12 =      0.1425;
    p03 =   -0.002106;
    p40 =  -6.009e-05;
    p31 =   2.473e-05;
    p22 =    -0.00735;
    p13 =      0.1953;
    p50 =   -7.38e-05;
    p41 =    0.001731;
    p32 =     -0.2116;
    p23 =     0.03323;
    f2=p00 + p10.*beta + p01.*alpha + p20.*beta.^2 + p11.*beta.*alpha + p02.* ...
        alpha.^2 + p30.*beta.^3 + p21.*beta.^2.*alpha + p12.*beta.*alpha.^2 + ...
        p03.*alpha.^3 + p40.*beta.^4 + p31.*beta.^3.*alpha + p22.*beta.^2.* ...
        alpha.^2 + p13.*beta.*alpha.^3 + p50.*beta.^5 + p41.*beta.^4.*alpha + ...
        p32.*beta.^3.*alpha.^2 + p23.*beta.^2.*alpha.^3;
    rhiprho=f1+f2;
end
function [rhiptheta]=rhth(alpha,beta)
% given torso alpha and beta euler angles, find angle position of right hip
% theta joint
    a0_f=  0.6746*alpha+0.3259;
    a1_f= -0.1186*alpha-0.3258;
    b1_f= -6.718e-5*alpha-2.882e-6;
    w_f = -0.5968*alpha+1.554; 
    f1=a0_f + a1_f.*cos(beta.*w_f) + b1_f.*sin(beta.*w_f);

    p00 =  -0.0003866;
    p10 =   -6.32e-07;
    p01 =    0.003776;
    p20 =   -0.001095;
    p11 =   0.0002509;
    p02 =     0.05658;
    p21 =    -0.00502;
    p12 =     0.00149;
    p03 =     -0.2953;
    p22 =     0.09049;
    p13 =    -0.02362;
    p04 =     -0.1541;
    f2=p00 + p10.*beta + p01.*alpha + p20.*beta.^2 + p11.*beta.*alpha + p02.* ...
        alpha.^2 + p21.*beta.^2.*alpha + p12.*beta.*alpha.^2 + p03.*alpha.^3 + ...
        p22.*beta.^2.*alpha.^2 + p13.*beta.*alpha.^3 + p04.*alpha.^4;
    rhiptheta=f1+f2;
end
function [rtransfem]=rtf(alpha,beta)
% given torso alpha and beta euler angles, find angle position of right
% transverse femeral joint
    a0_f= 2.561*alpha-0.4379;
    a1_f=-3.952*alpha+0.6677;
    a2_f= 1.723*alpha-0.2838;
    a3_f=-0.3334*alpha+0.0523;
    b1_f= 3.73*alpha-0.8686;
    b2_f=-2.988*alpha+0.1733;
    b3_f= 1.263*alpha-0.07851;
    f1=a0_f + a1_f.*cos(beta) + b1_f.*sin(beta) + a2_f.*cos(2*beta) + b2_f.*sin(2*beta) ...
        + a3_f.*cos(3*beta) + b3_f.*sin(3*beta);

    p00 =   0.0002306;
    p10 =   0.0005368;
    p01 =   -0.004998;
    p20 =  -0.0005887;
    p11 =     0.01529;
    p02 =     0.03809;
    p21 =    0.004088;
    p12 =    -0.07505;
    p03 =      0.3255;
    p22 =   -0.005519;
    p13 =      -1.212;
    p04 =      -3.164;
    f2=p00 + p10.*beta + p01.*alpha + p20.*beta.^2 + p11.*beta.*alpha + p02.* ...
        alpha.^2 + p21.*beta.^2.*alpha + p12.*beta.*alpha.^2 + p03.*alpha.^3 + ...
        p22.*beta.^2.*alpha.^2 + p13.*beta.*alpha.^3 + p04.*alpha.^4;
    rtransfem=f1+f2;
end