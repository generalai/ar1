% USE FK_TEST.TTT TO RUN THIS TEST SCRIPT
% test_ang=1.0077;
% delta_ang=[1.002 -1.0106 1.0171 -1.0123 1.0008 -0.9951 0.9985 -1.0007 test_ang -test_ang test_ang];
%% Produce base data set with initial angle=-0.6643
delta_ang_orig=[0 0 0 0 0];% .0266002
angle_const_range= [ 
[-4.9:.5:5];
[-4.8:.5:5];
[-4.7:.5:5];
[-4.6:.5:5]; 
];
angle_const_rows=size(angle_const_range,1);
angle_incr_range=[5:-.5:-4];
angle_const=3;% delta_ang index to hold constant
angle_incr=2;% delta_ang index to increment sequentially
angle_control=1;% delta_ang index to modify in order to get desired dynamics
init_data_set=1;
%% Produce second data set with inital angle=-.664
% ainit=0.266766;
% delta_ang_orig=[ainit 0 -2 2 0];% .0266766
% angle_const_range= ones(1,10)*-2;
% angle_incr_range=[0];
% angle_const=3;% delta_ang index to hold constant
% angle_incr=2;% delta_ang index to increment sequentially
% angle_control=1;% delta_ang index to modify in order to get desired dynamics
% init_data_set=0;
%%
run_time=1.32*(length(delta_ang_orig)+1);
% simulation runs at 100 Hz
simtime_incr=.01;
% EBPM=[];% record euler beta pelvis signal from each trial in an array
% ANKPM=[];% record ankle pitch angle signal from each trial in an array
% AM=[];% record angle command vector from each trial in an array
% HIPBCOM=[];
counter1=15;
tic
for  cur_ang_incr=angle_incr_range
    counter1=counter1+1;    
    counter2=0;
    for cur_ang_cons_row=1:angle_const_rows
        a1diffcount=0;
        for cur_ang_cons=angle_const_range(cur_ang_cons_row,:)
            %% Initialize variables
            counter2=counter2+1;
            counter3=0;
            delta_ang=delta_ang_orig;
            control_angle_found=0;% has a control angle been found that produces the desired dynamics?
            vel_sign=NaN;
            measure_time_idx=337:437;% time index to measure signals of interest
            evaluate_time_idx=600:790;% time index to examine system dynamics
            test_idx=measure_time_idx(1):evaluate_time_idx(end);
            zero_limit=1e-5;
            a1diffcount=a1diffcount+1;
            delta_ang(angle_incr)=cur_ang_incr;
            delta_ang(angle_const)=cur_ang_cons;
            delta_ang(angle_const+1)=-cur_ang_cons;
            delta_ang(angle_const+2)=0;
            if init_data_set ==1
                min_incr=.0000001;
                hipb_initial_angle=-0.6643;
                if a1diffcount==1
                    cur_places=1;
                    init_angle=delta_ang(angle_control);
                elseif a1diffcount==2
                    cur_places=.01;
                    init_angle=AM(end,angle_control+1);
                elseif a1diffcount>2
                    cur_places=.000001;
                    init_angle=AM(end,angle_control+1)+diff(AM(end-1:end,angle_control+1));
                end
            else
                min_incr=.0000001;
                hipb_initial_angle=-0.6643+(.01 -(a1diffcount-1)*.02/length(angle_const_range));
                if a1diffcount==1
                    cur_places=1;
                    init_angle=delta_ang(angle_control);
                elseif a1diffcount==2
                    cur_places=1;
                    init_angle=AM(end,angle_control+1);
                elseif a1diffcount>2
                    cur_places=1;
                    init_angle=AM(end,angle_control+1)+diff(AM(end-1:end,angle_control+1));
                end
            end
            cur_mult=1;
            cur_delta=cur_places*cur_mult;
            cur_angle_delta=0;
            %% Optimize control angle
            while ~control_angle_found
                %% Configure initial control signals
                counter3=counter3+1;
                new_angle=init_angle+cur_angle_delta;
                delta_ang(angle_control)=new_angle;
                secs=1;% period of pulse train cycle
                delay_vector=[1 1 4 1]*secs;
                acc_segment_length_vector=[3 6 3 4]*secs;
                ang=[ 16 5 61 0 ]*0;
                pulse_group_delay=1;
                LKNEE = f_generate_pulse_train(acc_segment_length_vector,delay_vector, ...
                    pulse_group_delay,ang);
                
                secs=1;% period of pulse train cycle
                delay_vector=[1 1 4 1]*secs;
                acc_segment_length_vector=[3 6 3 4]*secs;
                ang=[ 16 5 61 0 ]*0;
                pulse_group_delay=1;
                RKNEE = f_generate_pulse_train(acc_segment_length_vector,delay_vector, ...
                    pulse_group_delay,ang);
                
                secs=2;% period of pulse train cycle
                delay_vector=[1 1]*secs;
                acc_segment_length_vector=[10 10]*secs;
                ang=[-15 30]*0;
                pulse_group_delay=1;
                LLEG = f_generate_pulse_train(acc_segment_length_vector,delay_vector, ...
                    pulse_group_delay,ang);
                
                secs=1;% period of pulse train cycle
                delay_vector=[1 1]*secs;
                acc_segment_length_vector=[5 5]*secs;
                ang=[-15 30]*0;
                pulse_group_delay=1;
                RLEG = f_generate_pulse_train(acc_segment_length_vector,delay_vector, ...
                    pulse_group_delay,ang);
                
                secs=1;% period of pulse train cycle
                delay_vector=[1 36 36 36 36 36]*secs;
                acc_segment_length_vector=[5 7 7 7 7 7]*secs;
                A=1;
                ang=[A .06 .69 .12 .7 .1];
                pulse_group_delay=1;
                HIPA = f_generate_pulse_train(acc_segment_length_vector,delay_vector, ...
                    pulse_group_delay,ang);
                
                secs=1;% period of pulse train cycle
                delay_vector=[10]*secs;
                acc_segment_length_vector=[7]*secs;
                ang=[hipb_initial_angle];
                pulse_group_delay=[];
                HIPB = f_generate_pulse_train(acc_segment_length_vector,delay_vector, ...
                    pulse_group_delay,ang);
                %% Plot control
                plot_results=1;
                %         if plot_results==1
                %             clf
                %             subplot(3,1,1)
                %             hold
                %             set(gca, 'color', [0 0 0],'Xcolor','w','Ycolor','w');
                %             xlabel ('time')
                %             subplot(3,1,2)
                %             hold
                %             set(gca, 'color', [0 0 0],'Xcolor','w','Ycolor','w');
                %             xlabel ('time')
                %             subplot(3,1,3)
                %             hold
                %             set(gca, 'color', [0 0 0],'Xcolor','w','Ycolor','w');
                %             set(gcf, 'color', [0 0 0]);
                %             xlabel ('time')
                %         end
                %% Initialize variables
                JOINT_ANGLES=[];
                JOINT_ANGLES.lhiprho =0;
                JOINT_ANGLES.lhiptheta = 0;
                JOINT_ANGLES.ltransfem = 0;
                JOINT_ANGLES.rhiprho = 0;
                JOINT_ANGLES.rhiptheta = 0;
                JOINT_ANGLES.rtransfem = 0;
                simtime=0;
                current_time_idx=0;
                gait_mode='standing_initialize';
                EBPMres=[];% record results for each time step
                ANKPMres=[];
                AMres=[];% record angle deltas for each time step
                HIPBCOMres=[];% record command signal sent to hip
                acc=[];
                vel=[];
                RES=[];% temp variable: results
                COM=[];% temp variable: comamnd signal
                %% Initialize VREP
                vrep=remApi('remoteApi'); % using the prototype file (remoteApiProto.m)
                vrep.simxFinish(-1); % just in case, close all opened connections
                clientID=vrep.simxStart('127.0.0.1',19997,true,true,5000,5);
                vrep.simxStartSimulation(clientID,vrep.simx_opmode_oneshot_wait);
                vrep.simxSynchronous(clientID,true);
                objectType=vrep.sim_object_joint_type;
                operationMode=vrep.simx_opmode_oneshot_wait;
                dataType=0;
                [returnCode,jointHandles,intData,floatData,jointNames]=vrep.simxGetObjectGroupData(clientID,objectType,dataType,operationMode);
                jointObj=containers.Map(jointNames(:,1)',jointHandles);
                objectType=vrep.sim_object_shape_type;
                [returnCode,shapeHandles,intData,floatData,shapeNames]=vrep.simxGetObjectGroupData(clientID,objectType,dataType,operationMode);
                shapeObj=containers.Map(shapeNames(:,1)',shapeHandles);
                operationMode=vrep.simx_opmode_oneshot;
                %% RUN
                while simtime<=(run_time)% time in seconds to run sim
                    current_time_idx=current_time_idx+1;
                    switch gait_mode
                        case 'standing_initialize'
                            % HIPB
                            if current_time_idx==(HIPB.start_time_idx-1)
                                % Calculate the next angle magnitude and dANG/dt
                                % Angle is measured in degrees
                                ang=HIPB.angle_magnitudes(HIPB.pulse_seq_idx);
                                prev_ang=HIPB.prev_angle;
                                HIPB.acc_differential=(ang-prev_ang)/HIPB.unit_position_mag(HIPB.pulse_seq_idx);
                            end % if time_step==(PULSE1.sense_time_index-1)
                            HIPB=f_generate_angle(HIPB,current_time_idx);
                            if current_time_idx==HIPB.end_time_idx
                                % then we've just finished with current pulse
                                pulse_group_delay=100;
                                AMres=[HIPB.angle_magnitudes delta_ang];
                                ang=cumsum([HIPB.angle_magnitudes delta_ang]);
                                ang=ang(2:end);
                                delay_vector=ones(1,length(ang))*pulse_group_delay;
                                acc_segment_length_vector=ones(1,length(ang))*7;
                                temp_acc=HIPB.in.acc;
                                temp_vel=HIPB.in.vel;
                                temp_pos=HIPB.in.pos;
                                HIPB = f_generate_pulse_train(acc_segment_length_vector,delay_vector, ...
                                    pulse_group_delay,ang);
                                HIPB.in.acc=temp_acc;
                                HIPB.in.vel=temp_vel;
                                HIPB.in.pos=temp_pos;
                                HIPB.start_time_idx=current_time_idx+pulse_group_delay+1;
                                HIPB.end_time_idx=(HIPB.start_time_idx-1)+HIPB.acc_prime_len_vector(HIPB.pulse_seq_idx);
                                HIPB.prev_angle=HIPB.in.pos;
                                gait_mode='standing_steady_state';
                                %How many pulses to run during stead state standing
                                SSS_last_pulse=length(ang);
                                SSS_pulse_count=0;
                            end
                        case 'standing_steady_state'
                            % HIPB
                            if current_time_idx==(HIPB.start_time_idx-1)
                                % Calculate the next angle magnitude and dANG/dt
                                % Angle is measured in degrees
                                ang=HIPB.angle_magnitudes(HIPB.pulse_seq_idx);
                                prev_ang=HIPB.prev_angle;
                                HIPB.acc_differential=(ang-prev_ang)/HIPB.unit_position_mag(HIPB.pulse_seq_idx);
                                SSS_pulse_count=SSS_pulse_count+1;
                            end % if time_step==(PULSE1.sense_time_index-1)
                            HIPB=f_generate_angle(HIPB,current_time_idx);
                            if current_time_idx==HIPB.end_time_idx
                                % then we've just finished with current pulse
                                if SSS_pulse_count<SSS_last_pulse
                                    HIPB.prev_angle=HIPB.in.pos;
                                    if HIPB.pulse_seq_idx ~= length(HIPB.delay_vector)
                                        HIPB.start_time_idx=HIPB.end_time_idx+HIPB.delay_vector(HIPB.pulse_seq_idx+1)+1;
                                        HIPB.end_time_idx=(HIPB.start_time_idx-1)+HIPB.acc_prime_len_vector(HIPB.pulse_seq_idx+1);
                                        HIPB.pulse_seq_idx=HIPB.pulse_seq_idx+1;
                                    else
                                        HIPB.pulse_seq_idx=1;
                                        HIPB.start_time_idx=HIPB.end_time_idx+HIPB.pulse_group_delay+1;
                                        HIPB.end_time_idx=(HIPB.start_time_idx-1)+HIPB.acc_prime_len_vector(HIPB.pulse_seq_idx);
                                    end
                                else
                                    %                         gait_mode='gait_initialize';
                                    %                         HIPA.start_time_idx=HIPB.end_time_idx+15;
                                    %                         HIPA.end_time_idx=(HIPA.start_time_idx-1)+HIPA.acc_prime_len_vector(1);
                                    %                         HIPA.pulse_seq_idx=1;
                                end
                            end
                        case 'standing_termination'
                        case 'gait_initialize'
                            % HIPA
                            if current_time_idx==(HIPA.start_time_idx-1)
                                % Calculate the next angle magnitude and dANG/dt
                                % Angle is measured in degrees
                                ang=HIPA.angle_magnitudes(HIPA.pulse_seq_idx);
                                prev_ang=HIPA.prev_angle;
                                HIPA.acc_differential=(ang-prev_ang)/HIPA.unit_position_mag(HIPA.pulse_seq_idx);
                            end % if time_step==(PULSE1.sense_time_index-1)
                            HIPA=f_generate_angle(HIPA,current_time_idx);
                            if current_time_idx==HIPA.end_time_idx
                                % then we've just finished with current pulse
                                HIPA.prev_angle=HIPA.in.pos;
                                if HIPA.pulse_seq_idx ~= length(HIPA.delay_vector)
                                    HIPA.start_time_idx=HIPA.end_time_idx+HIPA.delay_vector(HIPA.pulse_seq_idx+1)+1;
                                    HIPA.end_time_idx=(HIPA.start_time_idx-1)+HIPA.acc_prime_len_vector(HIPA.pulse_seq_idx+1);
                                    HIPA.pulse_seq_idx=HIPA.pulse_seq_idx+1;
                                end
                            end
                            %                     gait_mode='gait_steady_state';
                            
                            %                 if current_time_idx==1
                            %                     %turn on a weak motor in the ankle pitch joints
                            %                    [returnCode]=vrep.simxSetObjectIntParameter(clientID,jointObj('R_ANKLE_PITCH_JNT'),vrep.sim_jointintparam_motor_enabled,1,operationMode);
                            %                    [returnCode]=vrep.simxSetObjectIntParameter(clientID,jointObj('L_ANKLE_PITCH_JNT'),vrep.sim_jointintparam_motor_enabled,1,operationMode);
                            %                 end
                        case 'gait_steady_state'
                        case 'gait_termination'
                        case 'other'
                    end
                    
                    %     % HIPA
                    %     if current_time_idx==(HIPA.start_time_idx-1)
                    %         % Calculate the next angle magnitude and dANG/dt
                    %         % Angle is measured in degrees
                    %         ang=HIPA.angle_magnitudes(HIPA.pulse_seq_idx);
                    %         prev_ang=HIPA.prev_angle;
                    %         HIPA.acc_differential=(ang-prev_ang)/HIPA.unit_position_mag(HIPA.pulse_seq_idx);
                    %     end % if time_step==(PULSE1.sense_time_index-1)
                    %     HIPA=f_generate_angle(HIPA,current_time_idx);
                    %     if current_time_idx==HIPA.end_time_idx
                    %         % then we've just finished with current pulse
                    %         HIPA.prev_angle=HIPA.in.pos;
                    %         if HIPA.pulse_seq_idx ~= length(HIPA.delay_vector)
                    %             HIPA.start_time_idx=HIPA.end_time_idx+HIPA.delay_vector(HIPA.pulse_seq_idx+1)+1;
                    %             HIPA.end_time_idx=(HIPA.start_time_idx-1)+HIPA.acc_prime_len_vector(HIPA.pulse_seq_idx+1);
                    %             HIPA.pulse_seq_idx=HIPA.pulse_seq_idx+1;
                    %         else
                    %             HIPA.pulse_seq_idx=1;
                    %             HIPA.start_time_idx=HIPA.end_time_idx+HIPA.pulse_group_delay+1;
                    %             HIPA.end_time_idx=(HIPA.start_time_idx-1)+HIPA.acc_prime_len_vector(HIPA.pulse_seq_idx);
                    %         end
                    %     end
                    %     % LKNEE
                    %     if current_time_idx==(LKNEE.start_time_idx-1)
                    %         % Calculate the next angle magnitude and dANG/dt
                    %         % Angle is measured in degrees
                    %         ang=LKNEE.angle_magnitudes(LKNEE.pulse_seq_idx);
                    %         prev_ang=LKNEE.prev_angle;
                    %         LKNEE.acc_differential=(ang-prev_ang)/LKNEE.unit_position_mag(LKNEE.pulse_seq_idx);
                    %     end % if time_step==(PULSE1.sense_time_index-1)
                    %     LKNEE=f_generate_angle(LKNEE,current_time_idx);
                    %     if current_time_idx==LKNEE.end_time_idx
                    %         % then we've just finished with current pulse
                    %         LKNEE.prev_angle=LKNEE.in.pos;
                    %         if LKNEE.pulse_seq_idx ~= length(LKNEE.delay_vector)
                    %             LKNEE.start_time_idx=LKNEE.end_time_idx+LKNEE.delay_vector(LKNEE.pulse_seq_idx+1)+1;
                    %             LKNEE.end_time_idx=(LKNEE.start_time_idx-1)+LKNEE.acc_prime_len_vector(LKNEE.pulse_seq_idx+1);
                    %             LKNEE.pulse_seq_idx=LKNEE.pulse_seq_idx+1;
                    %         else
                    %             LKNEE.pulse_seq_idx=1;
                    %             LKNEE.start_time_idx=LKNEE.end_time_idx+LKNEE.pulse_group_delay+1;
                    %             LKNEE.end_time_idx=(LKNEE.start_time_idx-1)+LKNEE.acc_prime_len_vector(LKNEE.pulse_seq_idx);
                    %         end
                    %     end
                    %     % LLEG
                    %     if current_time_idx==(LLEG.start_time_idx-1)
                    %         % Calculate the next angle magnitude and dANG/dt
                    %         % Angle is measured in degrees
                    %         ang=LLEG.angle_magnitudes(LLEG.pulse_seq_idx);
                    %         prev_ang=LLEG.prev_angle;
                    %         LLEG.acc_differential=(ang-prev_ang)/LLEG.unit_position_mag(LLEG.pulse_seq_idx);
                    %     end % if time_step==(PULSE1.sense_time_index-1)
                    %     LLEG=f_generate_angle(LLEG,current_time_idx);
                    %     if current_time_idx==LLEG.end_time_idx
                    %         % then we've just finished with current pulse
                    %         LLEG.prev_angle=LLEG.in.pos;
                    %         if LLEG.pulse_seq_idx ~= length(LLEG.delay_vector)
                    %             LLEG.start_time_idx=LLEG.end_time_idx+LLEG.delay_vector(LLEG.pulse_seq_idx+1)+1;
                    %             LLEG.end_time_idx=(LLEG.start_time_idx-1)+LLEG.acc_prime_len_vector(LLEG.pulse_seq_idx+1);
                    %             LLEG.pulse_seq_idx=LLEG.pulse_seq_idx+1;
                    %         else
                    %             LLEG.pulse_seq_idx=1;
                    %             LLEG.start_time_idx=LLEG.end_time_idx+LLEG.pulse_group_delay+1;
                    %             LLEG.end_time_idx=(LLEG.start_time_idx-1)+LLEG.acc_prime_len_vector(LLEG.pulse_seq_idx);
                    %         end
                    %     end
                    % ACTUATION
                    JOINT_ANGLE_RK=RKNEE.in.pos*pi/180;
                    %     COM(end+1,:)=JOIN_ANGLE_LK;
                    JOINT_ANGLE_LK=LKNEE.in.pos*pi/180;
                    %     COM(end+1,:)=JOIN_ANGLE_LK;
                    alpha=HIPA.in.pos*pi/180;
                    beta=HIPB.in.pos*pi/180;
                    HIPBCOMres(end+1)=beta;
                    JOINT_ANGLES_B=f_inverse_kinematics(alpha,beta,'both_legs');
                    alpha=0;
                    beta=LLEG.in.pos*pi/180;
                    %     COM(end+1,:)=[alpha beta];
                    JOINT_ANGLES_L=f_inverse_kinematics(alpha,beta,'left_leg');
                    alpha=0;
                    beta=RLEG.in.pos*pi/180;
                    %     COM(end+1,:)=[alpha beta];
                    JOINT_ANGLES_R=f_inverse_kinematics(alpha,beta,'right_leg');
                    JOINT_ANGLES.lhiprho = JOINT_ANGLES_B.lhiprho + JOINT_ANGLES_L.lhiprho;
                    JOINT_ANGLES.lhiptheta = JOINT_ANGLES_B.lhiptheta + JOINT_ANGLES_L.lhiptheta;
                    JOINT_ANGLES.ltransfem = JOINT_ANGLES_B.ltransfem + JOINT_ANGLES_L.ltransfem;
                    JOINT_ANGLES.rhiprho = JOINT_ANGLES_B.rhiprho + JOINT_ANGLES_R.rhiprho;
                    JOINT_ANGLES.rhiptheta = JOINT_ANGLES_B.rhiptheta + JOINT_ANGLES_R.rhiptheta;
                    JOINT_ANGLES.rtransfem = JOINT_ANGLES_B.rtransfem + JOINT_ANGLES_R.rtransfem;
                    [returnCode]=vrep.simxSetJointTargetPosition(clientID, jointObj('R_HIP_RHO_JNT'), JOINT_ANGLES.rhiprho, operationMode);
                    [returnCode]=vrep.simxSetJointTargetPosition(clientID, jointObj('R_HIP_THETA_JNT'), JOINT_ANGLES.rhiptheta, operationMode);
                    [returnCode]=vrep.simxSetJointTargetPosition(clientID, jointObj('R_TRANS_FEM_JNT'), JOINT_ANGLES.rtransfem, operationMode);
                    [returnCode]=vrep.simxSetJointTargetPosition(clientID, jointObj('L_HIP_RHO_JNT'), JOINT_ANGLES.lhiprho, operationMode);
                    [returnCode]=vrep.simxSetJointTargetPosition(clientID, jointObj('L_HIP_THETA_JNT'), JOINT_ANGLES.lhiptheta, operationMode);
                    [returnCode]=vrep.simxSetJointTargetPosition(clientID, jointObj('L_TRANS_FEM_JNT'), JOINT_ANGLES.ltransfem, operationMode);
                    [returnCode]=vrep.simxSetJointTargetPosition(clientID, jointObj('R_KNEE_JNT'), JOINT_ANGLE_RK, operationMode);
                    [returnCode]=vrep.simxSetJointTargetPosition(clientID, jointObj('L_KNEE_JNT'), JOINT_ANGLE_LK, operationMode);
                    vrep.simxSynchronousTrigger(clientID);
                    [returnCode,ranklepitch]=vrep.simxGetJointPosition(clientID,jointObj('R_ANKLE_PITCH_JNT'), vrep.simx_opmode_streaming);
                    [returnCode,lanklepitch]=vrep.simxGetJointPosition(clientID,jointObj('L_ANKLE_PITCH_JNT'), vrep.simx_opmode_streaming);
                    [returnCode,lhiprhopos]=vrep.simxGetJointPosition(clientID,jointObj('L_HIP_RHO_JNT'), vrep.simx_opmode_streaming);
                    [returnCode,lhipthetapos]=vrep.simxGetJointPosition(clientID,jointObj('L_HIP_THETA_JNT'), vrep.simx_opmode_streaming);
                    [returnCode,ltransfempos]=vrep.simxGetJointPosition(clientID,jointObj('L_TRANS_FEM_JNT'), vrep.simx_opmode_streaming);
                    [returnCode,rhiprhopos]=vrep.simxGetJointPosition(clientID,jointObj('R_HIP_RHO_JNT'), vrep.simx_opmode_streaming);
                    [returnCode,rhipthetapos]=vrep.simxGetJointPosition(clientID,jointObj('R_HIP_THETA_JNT'), vrep.simx_opmode_streaming);
                    [returnCode,rtransfempos]=vrep.simxGetJointPosition(clientID,jointObj('R_TRANS_FEM_JNT'), vrep.simx_opmode_streaming);
                    [returnCode,rknee]=vrep.simxGetJointPosition(clientID,jointObj('R_KNEE_JNT'), vrep.simx_opmode_streaming);
                    [returnCode,lknee]=vrep.simxGetJointPosition(clientID,jointObj('L_KNEE_JNT'), vrep.simx_opmode_streaming);
                    [returnCode,eulerAngles]=vrep.simxGetObjectOrientation(clientID, shapeObj('TORSO_LNK'), -1, vrep.simx_opmode_streaming);
                    simtime=simtime+simtime_incr;
                    EBPMres(end+1)=eulerAngles(2);
                    ANKPMres(end+1)=ranklepitch;
                end% while simtime<=(10/simtime_incr)
                %% Shutdown VREP:
                vrep.simxStopSimulation(clientID,vrep.simx_opmode_oneshot_wait);
                % Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
                vrep.simxGetPingTime(clientID);
                % Now close the connection to V-REP:
                vrep.simxFinish(clientID);
                vrep.delete(); % call the destructor!	8
                disp('Program ended');
                %% Adjust control angle
                % Print Status
                [{'Const Cnt' 'Incr Cnt' 'Contr Cnt'  'cur_delta'}; num2cell([counter2 counter1  counter3  cur_delta])]
                da=diff([0 ANKPMres]);
                prev_vel_sign=vel_sign;
                gt_thresh_idx=find(da(test_idx)>.01,1);
                lt_thresh_idx=find(da(test_idx)<-.01,1);
                if isempty(lt_thresh_idx) && isempty(gt_thresh_idx)
                    if da(test_idx(end))>=0
                        gt_thresh_idx=0;
                        lt_thresh_idx=1;
                    elseif da(test_idx(end))<0
                        lt_thresh_idx=0;
                        gt_thresh_idx=1;
                    end
                elseif isempty(lt_thresh_idx)
                    lt_thresh_idx=gt_thresh_idx+1;
                elseif isempty(gt_thresh_idx)
                    gt_thresh_idx=lt_thresh_idx+1;
                end
                if gt_thresh_idx<lt_thresh_idx
                    vel_sign=1;
                    if (vel_sign==-prev_vel_sign)
                        if cur_mult==5
                            cur_mult=1;
                        elseif cur_mult==1
                            cur_places=cur_places*.1;
                            cur_mult=5;
                        end
                    elseif cur_mult==5
                        cur_mult=1;
                    end
                    cur_delta=cur_places*cur_mult;
                    if abs(cur_delta)<min_incr
                        cur_angle_delta=cur_angle_delta+min_incr;
                    else
                        cur_angle_delta=round((cur_angle_delta+cur_delta)/min_incr)*min_incr;
                    end
                elseif gt_thresh_idx>lt_thresh_idx
                    vel_sign=-1;
                    if (vel_sign==-prev_vel_sign)
                        if cur_mult==5
                            cur_mult=1;
                        elseif cur_mult==1
                            cur_places=cur_places*.1;
                            cur_mult=5;
                        end
                    elseif cur_mult==5
                        cur_mult=1;
                    end
                    cur_delta=cur_places*cur_mult;
                    if abs(cur_delta)<min_incr
                        cur_angle_delta=cur_angle_delta-min_incr;
                    else
                        cur_angle_delta=round((cur_angle_delta-cur_delta)/min_incr)*min_incr;
                    end
                end
                if delta_ang(angle_control)==0
                    last_angle=0;
                else
                    last_angle=delta_ang(end-1);
                end
                if (all(abs(da(10:end))<=zero_limit)) ||(((vel_sign==-prev_vel_sign) &&((vel_sign==(-1*sign(last_angle))) || (last_angle==0))) && (cur_delta<min_incr))
                    control_angle_found=1;
                end
                %% Plot results
                if plot_results==1
                    clf
                    subplot(2,1,1)
                    grid on
                    plot(da(5:end))
                    subplot(2,1,2)
                    grid on
                    plot(da(end-230:end),'.')
                    pause(.00001)
                    %                 subplot(3,1,1)
                    %                 %     plot(current_time_idx,LKNEE.in.acc,'r.')
                    %                 plot(current_time_idx,HIPB.in.acc,'m.')
                    %                 plot(current_time_idx,HIPA.in.acc,'w.')
                    %                 %     plot(current_time_idx,PULSE1.in.acc+PULSE2.in.acc,'r.')
                    %                 %     acc(end+1)=PULSE1.in.acc+PULSE2.in.acc;
                    %                 %         acc(end+1)=LKNEE.in.acc;
                    %                 subplot(3,1,2)
                    %                 %     plot(current_time_idx,LKNEE.in.vel,'g.')
                    %                 plot(simtime,HIPB.in.vel,'g.')
                    %                 plot(simtime,HIPA.in.vel,'w.')
                    %                 %     plot(current_time_idx,PULSE1.in.vel+PULSE2.in.vel,'g.')
                    %                 subplot(3,1,3)
                    %                 %     vel(end+1)=PULSE1.in.vel+PULSE2.in.vel;
                    %                 %     plot(current_time_idx,LKNEE.in.pos,'b.' )
                    %                 %         plot(simtime,HIPB.in.pos,'b.' )
                    %                 %         plot(simtime,HIPA.in.pos,'w.' )
                    %                 plot(current_time_idx,RES(end,1)-RES(end-1,1),'w.')
                    %                 pause(.000001)
                end
            end% while ~control_angle_found
            %% Data analysis
            % Data to colloect:
            % delta_ang
            % Ankle pitch angle signal between last 2 pulses
            % Torso euler angle beta signal between last 2 pulses
            
            
            ANKPMres(1)=ANKPMres(2);% eliminate initial transient spike
            ANKPM(end+1,:)=ANKPMres;%(measure_time_idx);
            EBPM(end+1,:)=EBPMres;%(measure_time_idx);
            HIPBCOM(end+1,:)=HIPBCOMres;
            AM(end+1,:)=AMres;
        end% for cur_ang_incr=angle_incr_range
    end% for cur_ang_cons_row=2:angle_const_rows
end% for cur_ang_cons=angle_const_range
disp('FINISHED')
toc
%%
am=[];
ankp=[];
ankv=[];
anka=[];
ebpp=[];
ebpv=[];
ebpa=[];
idx=337:437;
am=[AM(:,3+1)];% rememebr to add 1 to account for initial angle pulse
ankp=ANKPM(:,idx(end));
ankv=ANKPM(:,idx(end))-ANKPM(:,idx(end-1));
anka=(ANKPM(:,idx(end))-ANKPM(:,idx(end-1)))-(ANKPM(:,idx(end-1))-ANKPM(:,idx(end-2)));
ebpp=EBPM(:,idx(end));
ebpv=EBPM(:,idx(end))-EBPM(:,end-1);
ebpa=(EBPM(:,idx(end))-EBPM(:,idx(end-1)))-(EBPM(:,idx(end-1))-EBPM(:,idx(end-2)));
idx=find((AM(:,4)<=2).*(AM(:,4)>=-2).*(AM(:,3)<=5).*(AM(:,3)>=-1.5));
idx=find((round(AM(:,3)*10)/10==5).*(round(AM(:,4)*10)/10==-4.5)); %-*4.4 4.6
idx=1:size(AM,1);
x=ankp;
y=ankv;
z=am;
clf
plot3(x(idx),y(idx),z(idx),'r.')
hold
%  plot3(x(idx),y(idx),z(idx),'o')
 %%
% idx=find(y<-2e-5)
% 
% AM(idx,:)=[];
% ANKPM(idx,:)=[];
% EBPM(idx,:)=[];
